const test4 = () => {
    name = 'test4';
    console.log(name + ' start')
    const gamemasters = [
        { id: 1, name: 'John', trained_rooms: [1, 2, 3] },
        { id: 2, name: 'Alice', trained_rooms: [1] },
        { id: 3, name: 'David', trained_rooms: [2] },
    ];

    const sessions =
        [
            { id: 1, name: "Le Braquage à la francaise" },
            { id: 2, name: "Le Braquage de casino" },
            { id: 3, name: "L'Enlèvement" },
        ]
            .map(room => {return {room: room}});

    console.log('gamemasters', gamemasters);
    console.log('sessions', sessions);

    const solution = attribution(gamemasters, sessions);
    console.log(solution);

    const expected = [
        {
            "roomId": 3,
            "masterId": 1
        },
        {
            "roomId": 1,
            "masterId": 2
        },
        {
            "roomId": 2,
            "masterId": 3
        },
    ];

    if (JSON.stringify(expected) === JSON.stringify(solution)) {
        document.querySelector('#test').innerHTML += '<div>' + name + ' : <span style="color: green">OK</span></div>';
    } else {
        document.querySelector('#test').innerHTML += '<div>' + name + ' : <span style="color: red">KO</span></div>';
    }

    console.log(name + ' end');
}