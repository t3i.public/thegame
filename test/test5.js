const test5 = () => {
    name = 'test5';
    console.log(name + ' start')
    const gamemasters = [
        {
            "id": 11,
            "name": "James",
            "trained_rooms": [
                11
            ]
        },
        {
            "id": 14,
            "name": "Ava",
            "trained_rooms": [
                9
            ]
        },
        {
            "id": 13,
            "name": "William",
            "trained_rooms": [
                11
            ]
        },
        {
            "id": 2,
            "name": "Alice",
            "trained_rooms": [
                4,
                10
            ]
        },
        {
            "id": 20,
            "name": "Harper",
            "trained_rooms": [
                1,
                12
            ]
        },
        {
            "id": 1,
            "name": "John",
            "trained_rooms": [
                2,
                3
            ]
        },
        {
            "id": 10,
            "name": "Emma",
            "trained_rooms": [
                5,
                4
            ]
        },
        {
            "id": 19,
            "name": "Alexandre",
            "trained_rooms": [
                9,
                2,
                8
            ]
        },
        {
            "id": 4,
            "name": "Emily",
            "trained_rooms": [
                8,
                6,
                2,
                7
            ]
        },
        {
            "id": 16,
            "name": "Mia",
            "trained_rooms": [
                1,
                3,
                7,
                5,
                8
            ]
        },
        {
            "id": 9,
            "name": "Matthew",
            "trained_rooms": [
                2,
                6,
                1,
                7,
                3,
                4
            ]
        },
        {
            "id": 5,
            "name": "Michael",
            "trained_rooms": [
                9,
                1,
                4,
                3,
                11,
                8,
                6,
                12
            ]
        }
    ];

    const sessions = [
        { id: 1, name: "Le Braquage à la francaise" },
        { id: 2, name: "Le Braquage de casino" },
        { id: 3, name: "L'Enlèvement" },
        { id: 4, name: "Le Métro" },
        { id: 5, name: "Les Catacombes" },
        { id: 6, name: "Assassin's Creed" },
        { id: 7, name: "L'Avion" },
        { id: 8, name: "La Mission spatiale" },
        { id: 9, name: "Le Tremblement de terre" },
        { id: 10, name: "Le Cinéma hanté" },
        { id: 11, name: "Le Farwest" },
        { id: 12, name: "Mission secrète" }
    ]
            .map(room => {return {room: room}});

    console.log('gamemasters', gamemasters);
    console.log('sessions', sessions);

    const solution = attribution(gamemasters, sessions);
    console.log(solution);

    const expected = 'time out';

    if (JSON.stringify(expected) === JSON.stringify(solution)) {
        document.querySelector('#test').innerHTML += '<div>' + name + ' : <span style="color: green">OK</span></div>';
    } else {
        document.querySelector('#test').innerHTML += '<div>' + name + ' : <span style="color: red">KO</span></div>';
    }

    console.log(name + ' end');
}