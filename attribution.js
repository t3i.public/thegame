const attribution = (gamemasters, sessions) => {

    // order the game master by nb rooms they are trained
    gamemasters.sort((a, b) => a.trained_rooms.length - b.trained_rooms.length);


    // get rooms envolve
    const rooms = sessions.map((session) => session.room.id);

    // add gamemaster to session
    gamemasters.forEach(
        (master) => master.trained_rooms.forEach(
            (masterRoomId) => {
                if (!rooms.includes(masterRoomId)) {
                    return;
                }
                (sessions.find((session) => session.room.id === masterRoomId).room.gamemasters ||= []).push(master);
            }
        ));

    // check if there are room without gamemaster
    const emptyRoom = sessions.find((session) => !session.room.gamemasters);
    if (emptyRoom) {
        console.log('empty room', emptyRoom);
        return 'empty room';
    }

    // order session by nb of gamemaster.
    sessions.sort((a, b) => a.room.gamemasters.length - b.room.gamemasters.length);

    let selectionId = [];
    let indexes = [];
    let minIndex = 0;
    let t = 0;
    while(selectionId.length < sessions.length) {

        console.log('');
        console.log('t', t);
        console.log('size', selectionId.length);
        console.log('minIndex', minIndex);

        const room = sessions[selectionId.length].room;
        const index = room.gamemasters.findIndex(
            (gamemaster, i) =>
                !selectionId.includes(gamemaster.id)
                && minIndex <= i
        );

        console.log('index', index);
        const gamemasterId = room.gamemasters[index]?.id;

        if (gamemasterId) {
            selectionId.push(gamemasterId);
            indexes.push(index);
            minIndex = 0;
        } else {

            if (selectionId.length === 1) {
                console.log('no solution', selectionId);
                return 'no solution';
            }

            console.log('change branch');
            selectionId.pop();
            indexes.pop();
            minIndex = indexes.slice(-1)[0] + 1;
            indexes[indexes.length - 1] = minIndex;
        }

        if (1000 < t++) {
            console.log('time out');
            console.log('selection', selectionId);
            return 'time out';
        }

    }

    console.log('final selection', selectionId);
    // console.log('indexes', indexes);
    const solution = sessions.map((session, i) => ({roomId: session.room.id, masterId:selectionId[i]}));
    // solution.sort((a, b) => a.roomId - b.roomId);
    return solution;
}